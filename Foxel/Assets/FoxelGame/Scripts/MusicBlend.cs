﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MusicBlend : MonoBehaviour {

    public AudioMixerSnapshot surface, cave, tree;

    public float blendTime = 1.0f;


    public void PlaySurfaceMusic()
    {
        surface.TransitionTo(blendTime);
    }

    public void PlayCaveMusic()
    {
        cave.TransitionTo(blendTime);
       
    }

    public void PlayTreeMusic()
    {
        tree.TransitionTo(blendTime);

    }

}
