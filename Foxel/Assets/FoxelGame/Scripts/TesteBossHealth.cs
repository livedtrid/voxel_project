﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using System;

public class TesteBossHealth : MonoBehaviour {

    [SerializeField]
    Trigger2DEvent triggerEnter,triggerExit;



    private void OnTriggerEnter2D(Collider2D collision)
    {
        triggerEnter.Invoke(collision);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        triggerExit.Invoke(collision);
    }
}
