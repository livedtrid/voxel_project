﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSFX : MonoBehaviour
{

    AudioSource audioSource;

    public AudioClip fireSFX, stompSFX, deathSFX, clapSFX, hurt;

    // Use this for initialization
    void Start()
    {
        audioSource = GetComponent<AudioSource>();

    }

    public void PlayFireSound()
    {
        audioSource.PlayOneShot(fireSFX);
    }

    public void PlayStompSound()
    {
        audioSource.PlayOneShot(stompSFX);
    }

    public void PlayDeathSound()
    {
        audioSource.PlayOneShot(fireSFX);
    }

    public void PlayClapSFX()
    {
        audioSource.PlayOneShot(clapSFX);
    }

    public void PlayHurtSFX()
    {
        audioSource.PlayOneShot(hurt);

    }
}
