﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class CameraShake : MonoBehaviour
{
    /// Shake parameters : intensity, duration (in seconds) and decay
    public Vector3 ShakeParameters = new Vector3(1.5f, 0.5f, 1f);


    public void DoShake()
    {

        Camera.main.GetComponent<CameraController>().Shake(ShakeParameters);
    }
}
