﻿using System;
using UnityEngine.Events;

[Serializable]
public class WeaponPickupEvent : UnityEvent<int> {}
